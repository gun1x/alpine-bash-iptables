if ip route | grep default | grep -q " $GATEWAY "; then
  echo gateway already good;
else
  ip route add "$GATEWAY" dev eth0 src "$IP"
  ip route del default
  ip route add default via "$GATEWAY"
fi
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
iptables -t nat -A PREROUTING -i eth0 -d "$IP"/32 -p tcp --match multiport --dports 8080,8081,8443 -j REDIRECT --to-port 443
iptables -t nat -A PREROUTING -i eth0 -d "$IP"/32 -p udp --match multiport --dports 8080,8081,8443 -j REDIRECT --to-port 443
iptables -t mangle -A FORWARD -m policy --pol ipsec --dir in -p tcp -m tcp --tcp-flags SYN,RST SYN -m tcpmss --mss 1361:1536 -j TCPMSS --set-mss 1360
iptables -t mangle -A FORWARD -m policy --pol ipsec --dir out -p tcp -m tcp --tcp-flags SYN,RST SYN -m tcpmss --mss 1361:1536 -j TCPMSS --set-mss 1360
iptables -A FORWARD -i eth0 -j ACCEPT
iptables -A FORWARD -o eth0 -j ACCEPT
iptables -A FORWARD -i eth1 -j ACCEPT
iptables -A FORWARD -o eth1 -j ACCEPT
iptables -P FORWARD DROP
bash
