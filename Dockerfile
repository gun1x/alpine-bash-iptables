FROM alpine:edge
RUN apk add --no-cache bash iptables tcpdump

COPY start.sh /start.sh

ENTRYPOINT ["/bin/bash"]
CMD ["/start.sh"]
