#!/bin/bash
epoch="$(date +%s)"
docker build -t "registry.gitlab.com/gun1x/alpine-bash-iptables/$(git branch --show-current):${epoch}" .
docker push "registry.gitlab.com/gun1x/alpine-bash-iptables/$(git branch --show-current):${epoch}"
echo "The tag is ${epoch}"
